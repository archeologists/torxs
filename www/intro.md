---
title: "Intro"
output: html_document
---

# Welcome to ⛏️ToRXS⛏️, the app to go from a spreadsheet To R eXtraction Script!

For more information, see 📖https://sysrevving.com📖, 📚https://metabefor.opens.science📚, or ⛏️https://archeologists.opens.science⛏️.

-----
